#!/bin/bash

# start the service
#service tomcat7 restart

# configure the IPs for the job
# the job is run under "tomcat7" user, and the environmental variable are not available to that user
#export LOCAL_HOST_IP=`ip addr list ${NETWORK_INTERFACE} | grep "inet " | cut -d" " -f6 | cut -d"/" -f1`
#export SPARK_MASTER_IP=`getent hosts ${SPARK_MASTER} | awk '{ print $1 }'`
#export CASSANDRA_IP=`getent hosts ${CASSANDRA} | awk '{ print $1 }'`

#sed -e s/LOCAL_HOST_IP=.*$/LOCAL_HOST_IP=${LOCAL_HOST_IP}/ /home/spark-remote-submit.sh > /tmp1.sh
#sed -e s/SPARK_MASTER_IP=.*$/SPARK_MASTER_IP=${SPARK_MASTER_IP}/ /tmp1.sh > /tmp2.sh
#sed -e s/CASSANDRA_IP=.*$/CASSANDRA_IP=${CASSANDRA_IP}/ /tmp2.sh > /tmp.sh

#rm /tmp1.sh /tmp2.sh
#mv /tmp.sh /home/spark-remote-submit.sh
#chmod +x /home/spark-remote-submit.sh

#sed -e s/SPARK_MASTER_IP/${SPARK_MASTER_IP}/ /var/lib/tomcat7/conf/context.xml > /tmp1.xml
#sed -e s/CASSANDRA_IP/${CASSANDRA_IP}/ /tmp1.xml > /tmp2.xml
#mv /tmp2.xml /var/lib/tomcat7/conf/context.xml
#rm /tmp1.xml

# start the service
service tomcat8 start

# update the database connection string here


# to keep the container alive, start bash
#exec tail -f /var/log/tomcat7/catalina.out
/bin/bash

